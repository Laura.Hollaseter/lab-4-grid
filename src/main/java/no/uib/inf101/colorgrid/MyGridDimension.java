package no.uib.inf101.colorgrid;

// Implement the GridDimension interface
public class MyGridDimension implements GridDimension {
  private final int rows;
  private final int cols;

  public MyGridDimension(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }
}


