package no.uib.inf101.colorgrid;

// Les om records her: https://inf101.ii.uib.no/notat/mutabilitet/#record

/**
 * A CellPosition consists of a row and a column.
 *
 * @param row  the row of the cell
 * @param col  the column of the cell
 */
public record CellPosition(int row, int col) {

  // Ekstra konstruktør som lager objekter med standard-verdi
  public CellPosition(int row) {
    this(row, 0); // standard-verdi 0 for kolonne
  }

  // Metoder fungerer som i en vanlig klasse
  public CellPosition nextRow() {
    return new CellPosition(this.row + 1, this.col);
  }
}

// public final class CellPosition {

//     // Instansvariablene er final:
//     private final int row;
//     private final int col;
  
//     public CellPosition(int row, int col) {
//       this.row = row;
//       this.col = col;
//     }
  
//     public int getRow() {
//       return this.row;
//     }
  
//     public int getCol() {
//       return this.col;
//     }
  


//     // Men hvis vi ikke får lov å mutere objektet, hvordan går vi da
//     // til neste rad? Løsning: la den som kaller metoden ta imot et
//     // NYTT objekt i stedet for å mutere dette.
//     public CellPosition nextRow() {
//       return new CellPosition(this.row + 1, this.col);
//     }
//   }
  
