package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public final class ColorGrid implements IColorGrid {
    private final int rows;
    private final int cols;
    private Color[][] colors;

    public ColorGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.colors = new Color[rows][cols];
    }

    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }



  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellColors = new ArrayList<>();
    for (int i = 0; i < rows(); i++) {
        for (int j = 0; j < cols(); j++) {
            Color color = get(new CellPosition(i, j)); // Use get method to handle nulls
            cellColors.add(new CellColor(new CellPosition(i, j), color)); // This will add a CellColor object to the list
        }
    }
    return cellColors;
}


    @Override
    public Color get(CellPosition pos) {
        if (pos.row() < 0 || pos.row() >= rows || pos.col() < 0 || pos.col() >= cols) {
            throw new IndexOutOfBoundsException("Position out of bounds");
        }
        return colors[pos.row()][pos.col()]; // Returns null if not set
    }
    

    @Override
    public void set(CellPosition pos, Color color) {
        if (pos.row() >= rows || pos.col() >= cols || pos.row() < 0 || pos.col() < 0) {
            throw new IndexOutOfBoundsException("Position is out of bounds");
        }
        colors[pos.row()][pos.col()] = color;
    }
}
