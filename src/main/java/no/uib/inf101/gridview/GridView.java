package no.uib.inf101.gridview;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;
//import no.uib.inf101.colorgrid.MyCellColorCollection;
import no.uib.inf101.colorgrid.MyGridDimension;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class GridView extends JPanel{
    private IColorGrid colorGrid;
    private IColorGrid grid; // Rutenettet som skal tegnes
    private double margin; // Marginen rundt rutenettet
    private Rectangle2D box; // Boksen som inneholder rutenettet
    


    public GridView() {
      // Assuming a 4x3 grid as per the illustration
      int gridRows = 3;
      int gridCols = 4;
      this.grid = new ColorGrid(gridRows, gridCols); // Replace ColorGrid with your IColorGrid implementation if different
  
      // Now set the specific colors as per the illustration
      this.grid.set(new CellPosition(0, 0), Color.RED);
      this.grid.set(new CellPosition(0, 3), Color.BLUE);
      this.grid.set(new CellPosition(2, 0), Color.YELLOW);
      this.grid.set(new CellPosition(2, 3), Color.GREEN);
  
      // Assuming the rest of the cells are defaulting to null which should be interpreted as gray
  
      this.box = new Rectangle2D.Double(0, 0, 400, 300); // Boksen som inneholder rutenettet
      this.margin = 10; // Marginen rundt rutenettet
      this.setPreferredSize(new Dimension(400, 300)); // Set the preferred size for the JPanel
  }
  
  



  public GridView(IColorGrid grid) {
      this.grid = grid;
      this.box = new Rectangle2D.Double(0, 0, 400, 300);
      this.margin = 10;
      this.setPreferredSize(new Dimension(400, 300));
  }





@Override
protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    drawGrid((Graphics2D) g);
}


//jeg lurer på hvorfor testene failer med en gang jeg endrer denne metoden til public??????
private static void drawCells(Graphics2D g2, CellColorCollection colorGrid, CellPositionToPixelConverter converter) {
    for (int row = 0; row < colorGrid.rows(); row++) {
        for (int col = 0; col < colorGrid.cols(); col++) {
            Color color = colorGrid.get(new CellPosition(row, col));
            if (color == null) {
                color = Color.DARK_GRAY; // Default color if none is set
            }
            Rectangle2D cellRect = converter.getBoundsForCell(new CellPosition(row, col));
            g2.setColor(color);
            g2.fill(cellRect);
        }
    }
}


private static final double OUTERMARGIN = 30;
private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

private void drawGrid(Graphics2D g2) {
    // Create the outer rectangle based on the size of the component and the margin
    Rectangle2D outerRect = new Rectangle2D.Double(
        OUTERMARGIN, OUTERMARGIN,
        this.getWidth() - 2 * OUTERMARGIN,
        this.getHeight() - 2 * OUTERMARGIN
    );

    // Fill the background
    g2.setColor(MARGINCOLOR);
    g2.fill(outerRect);

    // Now draw the cells on top of the background
    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(
        outerRect, this.grid, OUTERMARGIN
    );
    drawCells(g2, this.grid, converter);
}


}





