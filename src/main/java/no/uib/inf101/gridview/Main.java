package no.uib.inf101.gridview;
import java.awt.Color;
import java.awt.Shape;
import java.util.List;
import javax.swing.JFrame;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.GridDimension;
import java.awt.geom.Rectangle2D;

public class Main {
  public static void main(String[] args) {
    // ColorGrid colorGrid = new ColorGrid(3, 4); // Initialize your grid with dimensions and colors
    // // ... Initialize your colorGrid with colors ...
    // RecordGraphics2D recordGraphics = new RecordGraphics2D();
    // CellPositionToPixelConverter converter = new CellPositionToPixelConverter(
    //     new Rectangle2D.Double(0, 0, 100, 100), // dummy values
    //     new GridDimension() { // This is the anonymous class implementation
    //         @Override
    //         public int rows() {
    //             return 3; // The number of rows in your grid
    //         }

    //         @Override
    //         public int cols() {
    //             return 4; // The number of columns in your grid
    //         }
    //     },
    //     10 // example margin
    // );

    // GridView gridView = new GridView(colorGrid); // Assuming GridView has a constructor that takes a ColorGrid
    // gridView.drawCells(recordGraphics, colorGrid, converter); // Replace with actual method to draw cells

    // // Now, inspect the recorded data
    // for (Shape shape : recordGraphics.getRecordedShapes()) {
    //     if (shape instanceof Rectangle2D) {
    //         Rectangle2D rect = (Rectangle2D) shape;
    //         System.out.println("Filled shape: " + rect);
    //     }
    // }
    // for (Color color : recordGraphics.getRecordedColors()) {
    //     System.out.println("Filled color: " + color);
    // }
}
}

    // ColorGrid grid = new ColorGrid(10, 10); // Example usage
    // grid.set(new CellPosition(0, 0), Color.RED); // Set color at position (0,0)
    // List<CellColor> cellColors = grid.getCells();
    // GridView canvas = new GridView();
    // JFrame frame = new JFrame();
    // frame.setTitle("INF101");
    // frame.setContentPane(canvas);
    // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // frame.pack();
    // frame.setVisible(true);
  

